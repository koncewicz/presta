<?php

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class Summary extends Module
{
    protected $paidOrdersCnt;
    protected $paidOrdersSum;
    protected $cartsCnt;
    protected $customersCnt;

    public function __construct()
    {
        $this->name = 'summary';
        $this->author = 'Marek Koncewicz';
        $this->version = '1.0.1';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Summary');
        $this->description = $this->l('Display summary of a performance.');
    }

    public function install()
    {
        return parent::install()
            && Configuration::updateValue('SUMMARY_NUMBER_OF_ORDERS', true)
            && Configuration::updateValue('SUMMARY_AMOUNT_OF_ORDERS', true)
            && Configuration::updateValue('SUMMARY_NUMBER_OF_BASKETS', true)
            && Configuration::updateValue('SUMMARY_NUMBER_OF_CLIENTS', true)
            && Configuration::updateValue('SUMMARY_SHOW_ON_LEFT_SIDE', true)
            && Configuration::updateValue('SUMMARY_SHOW_ON_RIGHT_SIDE', true)
            && Configuration::updateValue('SUMMARY_SHOW_ON_MAIN_PAGE', true)
            && $this->registerHook('actionMaintenancePageForm')
            && $this->registerHook('actionAdminMaintenanceControllerPostProcessBefore')
            && $this->registerHook('displayLeftColumn')
            && $this->registerHook('displayRightColumn')
            && $this->registerHook('header')
            && $this->registerHook('displayHome')
            ;
    }

    public function hookHeader()
    {
        $this->context->controller->addCSS($this->_path . 'public/module.css');
        $this->context->controller->addJS($this->_path . 'public/countUp.umd.js');
    }

    public function hookActionMaintenancePageForm(&$hookParams)
    {
        $formBuilder = $hookParams['form_builder'];
        $uploadQuotaForm = $formBuilder->get('general');

        $uploadQuotaForm->add(
            'show_number_of_orders',
            CheckboxType::class,
            [
                'required' => false,
                'data' => !!Configuration::get('SUMMARY_NUMBER_OF_ORDERS'),
                'label' => 'Show the number of orders'
            ]
        );

        $uploadQuotaForm->add(
            'show_amount_of_orders',
            CheckboxType::class,
            [
                'required' => false,
                'data' => !!Configuration::get('SUMMARY_AMOUNT_OF_ORDERS'),
                'label' => 'Show the amount of orders'
            ]
        );

        $uploadQuotaForm->add(
            'show_number_of_baskets',
            CheckboxType::class,
            [
                'required' => false,
                'data' => !!Configuration::get('SUMMARY_NUMBER_OF_BASKETS'),
                'label' => 'Show the number of baskets'
            ]
        );

        $uploadQuotaForm->add(
            'show_number_of_clients',
            CheckboxType::class,
            [
                'required' => false,
                'data' => !!Configuration::get('SUMMARY_NUMBER_OF_CLIENTS'),
                'label' => 'Show the number of clients'
            ]
        );

        $uploadQuotaForm->add(
            'show_on_left_side',
            CheckboxType::class,
            [
                'required' => false,
                'data' => !!Configuration::get('SUMMARY_SHOW_ON_LEFT_SIDE'),
                'label' => 'Show widget on left side'
            ]
        );

        $uploadQuotaForm->add(
            'show_on_right_side',
            CheckboxType::class,
            [
                'required' => false,
                'data' => !!Configuration::get('SUMMARY_SHOW_ON_RIGHT_SIDE'),
                'label' => 'Show widget on right side'
            ]
        );

        $uploadQuotaForm->add(
            'show_on_main_page',
            CheckboxType::class,
            [
                'required' => false,
                'data' => !!Configuration::get('SUMMARY_SHOW_ON_MAIN_PAGE'),
                'label' => 'Show widget on main page'
            ]
        );
    }

    public function hookActionAdminMaintenanceControllerPostProcessBefore($data)
    {
        Configuration::updateValue(
            'SUMMARY_NUMBER_OF_ORDERS',
            isset($data['request']->request->get('form')['general']['show_number_of_orders']) ? true : false
        );

        Configuration::updateValue(
            'SUMMARY_AMOUNT_OF_ORDERS',
            isset($data['request']->request->get('form')['general']['show_amount_of_orders']) ? true : false
        );

        Configuration::updateValue(
            'SUMMARY_NUMBER_OF_BASKETS',
            isset($data['request']->request->get('form')['general']['show_number_of_baskets']) ? true : false
        );

        Configuration::updateValue(
            'SUMMARY_NUMBER_OF_CLIENTS',
            isset($data['request']->request->get('form')['general']['show_number_of_clients']) ? true : false
        );

        Configuration::updateValue(
            'SUMMARY_SHOW_ON_LEFT_SIDE',
            isset($data['request']->request->get('form')['general']['show_on_left_side']) ? true : false
        );

        Configuration::updateValue(
            'SUMMARY_SHOW_ON_RIGHT_SIDE',
            isset($data['request']->request->get('form')['general']['show_on_right_side']) ? true : false
        );

        Configuration::updateValue(
            'SUMMARY_SHOW_ON_MAIN_PAGE',
            isset($data['request']->request->get('form')['general']['show_on_main_page']) ? true : false
        );
    }

    public function hookDisplayLeftColumn()
    {
        if (Configuration::get('SUMMARY_SHOW_ON_LEFT_SIDE')) {
            return $this->widget(['responsive' => true]);
        }
    }

    public function hookDisplayRightColumn()
    {
        if (Configuration::get('SUMMARY_SHOW_ON_RIGHT_SIDE')) {
            return $this->widget(['responsive' => true]);
        }
    }

    public function hookDisplayHome()
    {
        if (Configuration::get('SUMMARY_SHOW_ON_MAIN_PAGE')) {
            return $this->widget(['home' => true]);
        }
    }

    protected function widget($smartyVars = [])
    {
        $smartyVars['paidOrdersCnt'] = $this->getPaidOrdersCnt();
        $smartyVars['paidOrdersSum'] = $this->getPaidOrdersSum();
        $smartyVars['cartsCnt'] = $this->getCartsCnt();
        $smartyVars['customersCnt'] = $this->getCustomersCnt();
        $this->context->smarty->assign(['smartyVars' => $smartyVars]);

        return $this->display(__FILE__, 'displayLeftColumn.tpl');
    }

    protected function getPaidOrdersCnt()
    {
        if (!$this->paidOrdersCnt) {
            $sql = 'SELECT count(*) as cnt FROM `' . _DB_PREFIX_ . 'orders`
            WHERE
                total_paid_tax_incl = total_paid_real
                AND (date_add BETWEEN \'' . date('Y-m-d', time()) . ' 00:00:00\' AND \'' . date('Y-m-d', time()) . ' 23:59:59\')';
            $result = Db::getInstance()->getRow($sql);
            $this->paidOrdersCnt = $result['cnt'];
        }

        return $this->paidOrdersCnt;
    }

    protected function getPaidOrdersSum()
    {
        if (!$this->paidOrdersSum) {
            $sql = 'SELECT SUM(total_paid_real / conversion_rate) as sum FROM `' . _DB_PREFIX_ . 'orders`
            WHERE
                total_paid_tax_incl = total_paid_real
                AND (date_add BETWEEN \'' . date('Y-m-d', time()) . ' 00:00:00\' AND \'' . date('Y-m-d', time()) . ' 23:59:59\')';
            $result = Db::getInstance()->getRow($sql);

            $currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
            $this->paidOrdersSum = Tools::displayPrice($result['sum'] ?: 0, $currency);
        }

        return $this->paidOrdersSum;
    }

    protected function getCartsCnt()
    {
        if (!$this->cartsCnt) {
            $sql = 'SELECT count(*) as cnt FROM `' . _DB_PREFIX_ . 'cart`
            WHERE (date_add BETWEEN \'' . date('Y-m-d', time()) . ' 00:00:00\' AND \'' . date('Y-m-d', time()) . ' 23:59:59\')';
            $result = Db::getInstance()->getRow($sql);
            $this->cartsCnt = $result['cnt'];
        }

        return $this->cartsCnt;
    }

    protected function getCustomersCnt()
    {
        if (!$this->customersCnt) {
            $sql = 'SELECT count(*) as cnt FROM `' . _DB_PREFIX_ . 'customer`
            WHERE (date_add BETWEEN \'' . date('Y-m-d', time()) . ' 00:00:00\' AND \'' . date('Y-m-d', time()) . ' 23:59:59\')';
            $result = Db::getInstance()->getRow($sql);
            $this->customersCnt = $result['cnt'];
        }

        return $this->customersCnt;
    }
}
