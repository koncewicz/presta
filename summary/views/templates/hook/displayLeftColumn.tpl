{assign var="paidOrdersCnt" value=rand(1,1000000)}
{assign var="cartsCnt" value=rand(1,1000000)}
{assign var="customersCnt" value=rand(1,1000000)}

<div class="summary {if isset($smartyVars.responsive)}responsive{/if} {if isset($smartyVars.home)}home{/if}">
  <div class="header">
    {l s='Summary' mod='summary'}
  </div>
  <div class="clear"></div>

  <div class="order">
    {if Configuration::get('SUMMARY_NUMBER_OF_ORDERS')}
    <div class="ecommerce"></div>
    <div class="text">
      {l s='Today made' mod='summary'}
      <div class="green"><span id="orders-{$paidOrdersCnt}">{$smartyVars.paidOrdersCnt}</span> {l s='orders' mod='summary'}</div>
    </div>
    {/if}

    {if Configuration::get('SUMMARY_AMOUNT_OF_ORDERS')}
    <div class="quote">
      {l s='for value of' mod='summary'}
      <div class="green">{$smartyVars.paidOrdersSum}</div>
    </div>
    <div class="clear"></div>
    {/if}
  </div>

  <div class="hr"></div>

  {if Configuration::get('SUMMARY_NUMBER_OF_BASKETS')}
  <div class="cart">
    <div class="vertical left"></div>
    <div class="new"></div>
    <div class="text">
      {l s='Today created' mod='summary'}
      <div class="green"><span id="baskets-{$cartsCnt}">{$smartyVars.cartsCnt}</span> {l s='baskets' mod='summary'}</div>
    </div>
    <div class="vertical right"></div>
    <div class="clear"></div>
  </div>

  <div class="hr"></div>
  {/if}

  {if Configuration::get('SUMMARY_NUMBER_OF_CLIENTS')}
  <div class="user">
    <div class="followers"></div>
    <div class="text">
      <div class="green"><span id="customers-{$customersCnt}">{$smartyVars.customersCnt}</span> {l s='new customers' mod='summary'}</div>
      {l s='signed up today' mod='summary'}
    </div>
    <div class="clear"></div>
  </div>
  {/if}

  <div class="tick"></div>

  <div class="clear"></div>
</div>

<script>
  document.addEventListener("DOMContentLoaded", function(){
    (new countUp.CountUp('orders-{$paidOrdersCnt}', {$smartyVars.paidOrdersCnt})).start();
    (new countUp.CountUp('baskets-{$cartsCnt}', {$smartyVars.cartsCnt})).start();
    (new countUp.CountUp('customers-{$customersCnt}', {$smartyVars.customersCnt})).start();
  });

  var setResponse = function() {
    if (window.innerWidth <= 800 && document.getElementsByClassName('home').length > 0) {
      document.getElementsByClassName('home')[0].classList.add('responsive');
    }

    if (window.innerWidth > 800 && document.getElementsByClassName('home').length > 0) {
      document.getElementsByClassName('home')[0].classList.remove('responsive');
    }
  }

  window.onload = function() {
    setResponse();
  };

  window.onresize = function() {
    setResponse();
  };
</script>
